drop schema if exists safety_car;
create schema if not exists safety_car;
use safety_car;

create table car_brands
(
	id int auto_increment
		primary key,
	name varchar(20) not null
);

create table cars
(
	id int auto_increment
		primary key,
	car_brand_id int not null,
	model varchar(30) not null,
	constraint brand_models_car_brands_id_fk
		foreign key (car_brand_id) references car_brands (id)
);

create table multi_criteria_range
(
	id int auto_increment
		primary key,
	cc_min int not null,
	cc_max int not null,
	car_age_min int not null,
	car_age_max int not null,
	base_amount decimal(6,2) not null
);

create table offers
(
	id int auto_increment
		primary key,
	car_id int not null,
	cubic_capacity int not null,
	first_registration_date date not null,
	driver_age int not null,
	accidents_last_12_months tinyint(1) not null,
	total_premium decimal(6,2) not null,
	constraint simulate_offers_brand_models_id_fk
		foreign key (car_id) references cars (id)
);

create table status
(
	id int auto_increment
		primary key,
	value varchar(20) not null,
	constraint status_value_uindex
		unique (value)
);

create table users
(
	username varchar(50) not null
		primary key,
	password varchar(68) not null,
	enabled tinyint not null
);

create table authorities
(
	username varchar(50) not null,
	authority varchar(50) not null,
	constraint username_authority
		unique (username, authority),
	constraint authorities_fk
		foreign key (username) references users (username)
);

create table confirmation_tokens
(
	id int auto_increment
		primary key,
	confirmation_token varchar(100) null,
	created_date date null,
	username varchar(50) null,
	constraint confirmation_tokens_users_username_fk
		foreign key (username) references users (username)
);

create table user_details
(
	id int auto_increment
		primary key,
	email varchar(50) not null,
	first_name varchar(20) null,
	last_name varchar(20) null,
	enabled tinyint(1) not null,
	constraint user_details_email_uindex
		unique (email),
	constraint user_details_users_username_fk
		foreign key (email) references users (username)
);

create table requests
(
	id int auto_increment
		primary key,
	date date not null,
	phone varchar(10) not null,
	postal_address varchar(80) charset utf8 not null,
	vehicle_registration_certificate longblob not null,
	start_date date not null,
	status_id int not null,
	user_details_id int not null,
	offer_id int not null,
	constraint requests_offers_id_fk
		foreign key (offer_id) references offers (id),
	constraint requests_status_id_fk
		foreign key (status_id) references status (id),
	constraint requests_user_details_id_fk
		foreign key (user_details_id) references user_details (id)
);

