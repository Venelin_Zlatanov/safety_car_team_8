package com.team8.safetycar.exceptions;

import static com.team8.safetycar.utils.GlobalConstants.EMAIL_SEND_ERROR;

public class EmailNotSentException extends RuntimeException {

    public EmailNotSentException(String subject, String username) {
        super(String.format(EMAIL_SEND_ERROR, subject, username));
    }
}
