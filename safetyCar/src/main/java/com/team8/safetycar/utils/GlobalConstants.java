package com.team8.safetycar.utils;

public class GlobalConstants {

    public static final String EMAIL_REQUIRED_ERROR = "Email is required.";
    public static final String EMAIL_OR_PASSWORD_CAN_T_BE_EMPTY_ERROR = "Email/Password can't be empty!";
    public static final String EMAIL_INVALID_FORMAT_ERROR = "Not valid email format!";
    public static final String EMAIL_SUBJECT = "Email confirmation";
    public static final String EMAIL_CHANGED_STATUS_SUBJECT = "Status changed";
    public static final String EMAIL_SEND_ERROR = "Email with subject: %s cannot be sent to %s";

    public static final int USER_NAME_MIN_LENGTH = 2;
    public static final int USER_NAME_MAX_LENGTH = 50;
    public static final String USER_EMAIL_ALREADY_EXISTS_ERROR = "User with email %s already exists!";
    public static final String USER_FIRST_NAME_MESSAGE_ERROR = "First name should be between 2 and 50 symbols.";
    public static final String USER_LAST_NAME_MESSAGE_ERROR = "Last Name should be between 2 and 50 symbols.";
    public static final String USER_ID_NOT_FOUND_ERROR = "User with id %d not found!";
    public static final String USER_NAME_NOT_FOUND_ERROR = "User with username %s not found!";
    public static final String USER_NOT_ALLOWED_TO_EDIT_ERROR = "User %s is not allowed to edit user with id %d!";
    public static final int EMPTY_USERNAME_FILTER_VALUE = -1;

    public static final String PASSWORD_CANT_BE_EMPTY_ERROR = "Password can't be empty!";
    public static final String PASSWORD_CONFIRMATION_REQUIRED_MESSAGE_ERROR = "Confirm password is required.";
    public static final String USER_PASSWORD_MESSAGE_ERROR = "Password should be at least 8 characters and should contain " +
            "a minimum 1 lower case letter, 1 upper case letter, 1 numeric character and a special character: [@,#,$,%,!]";
    public static final String PASSWORD_DOESNT_MATCH_ERROR = "Password confirmation doesn't match password!";

    public static final String CORRECT_VALUE_MESSAGE_ERROR = "Please add correct value.";
    public static final String CAR_ID_AND_BRAND_ID_NOT_FOUND = "Car with id %d and brand id %d doesn't exist.";
    public static final String CAR_BRAND_ID_NOT_FOUND = "Car brand with id %d not found!";
    public static final String BRAND_WITH_ID_NOT_FOUND = "Car brand with id %d not found!";
    public static final String CAR_WITH_ID_NOT_FOUND = "Car with id %d not found!";
    public static final String CAR_BRAND_ID_ERROR = "Car brand is required.";
    public static final String CAR_MODEL_ID_ERROR = "Car model is required.";
    public static final String CUBIC_CAPACITY_ERROR = "Cubic capacity should be positive!";
    public static final String REGISTRATION_DATE_ERROR = "Please enter first registration date.";
    public static final String DRIVER_AGE_ERROR = "Driver age should be between then 18 and 99!";
    public static final int DRIVER_AGE_MIN = 18;
    public static final int DRIVER_AGE_MAX = 99;

    public static final String PHONE_ERROR = "Phone should be between 3 and 10 digits.";
    public static final String POSTAL_ADDRESS_ERROR = "Postal address should be between 3 and 80 symbols.";
    public static final String START_DATE_ERROR = "Please enter start date.";
    public static final int PHONE_LENGTH_MIN = 3;
    public static final int PHONE_LENGTH_MAX = 10;
    public static final int POSTAL_ADDRESS_LENGTH_MIN = 3;
    public static final int POSTAL_ADDRESS_LENGTH_MAX = 80;

    public static final String OFFER_ID_NOT_FOUND = "Offer with id %d not found!";
    public static final String REQUEST_ID_NOT_FOUND = "Request policy with id %d not found!";
    public static final String STATUS_ID_NOT_FOUND = "Status with id %d not found.";
    public static final String CHANGED_STATUS_MESSAGE_ERROR = "This request is already with changed status.";

    public static final String YOU_MUST_LOG_IN_MESSAGE = "You must log in first.";
    public static final String NOT_AUTHORIZED_ERROR = "You are not authorized to access this resource.";

    public static final double ACCIDENTS_LAST_12_MONTHS_COEFF = 0.2;
    public static final double DRIVER_AGE_BELOW_25_COEFF = 0.05;
    public static final double TAX_COEFF = 0.1;
    public static final int DRIVER_AGE = 25;


}
