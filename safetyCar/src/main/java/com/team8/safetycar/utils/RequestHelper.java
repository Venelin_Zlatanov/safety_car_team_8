package com.team8.safetycar.utils;


public class RequestHelper {

    public static Double roundTotalPremium(Double totalPremium) {
        return Math.round(totalPremium * 100.0) / 100.0;
    }
}
