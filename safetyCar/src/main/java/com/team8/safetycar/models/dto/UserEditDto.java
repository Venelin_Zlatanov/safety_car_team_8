package com.team8.safetycar.models.dto;

import javax.validation.constraints.Size;

import static com.team8.safetycar.utils.GlobalConstants.*;

public class UserEditDto {

    @Size(min = USER_NAME_MIN_LENGTH,
            max = USER_NAME_MAX_LENGTH,
            message = USER_FIRST_NAME_MESSAGE_ERROR)
    private String firstName;

    @Size(min = USER_NAME_MIN_LENGTH,
            max = USER_NAME_MAX_LENGTH,
            message = USER_LAST_NAME_MESSAGE_ERROR)
    private String lastName;

    public UserEditDto() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
