package com.team8.safetycar.models.mappers;

import com.team8.safetycar.exceptions.FileStorageException;
import com.team8.safetycar.models.Offer;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.Status;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.RequestDto;
import com.team8.safetycar.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.parse;

@Component
public class RequestMapper {

    private final StatusService statusService;

    @Autowired
    public RequestMapper(StatusService statusService) {
        this.statusService = statusService;
    }

    public Request toRequest(RequestDto requestDto,
                             Offer offer,
                             UserDetails userDetails) {
        try {
            Request request = new Request();
            request.setPhone(requestDto.getPhone());
            request.setPostalAddress(requestDto.getPostalAddress());

            MultipartFile file = requestDto.getVehicleRegistrationCertificate();
            request.setVehicleRegistrationCertificate(file.getBytes());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            request.setStartDate(parse(requestDto.getStartDate(), formatter));

            Status status = statusService.getByValue("pending");
            request.setStatus(status);

            request.setUserDetails(userDetails);
            request.setOffer(offer);
            return request;
        } catch (IOException | NullPointerException ex) {
            throw new FileStorageException(ex.getMessage());
        }
    }

    public void setStatus(Request request, int statusId) {
        Status status = statusService.getById(statusId);
        request.setStatus(status);
    }
}
