package com.team8.safetycar.models.dto;

public class StatusDto {

    String statusValue;

    public StatusDto() {
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }
}
