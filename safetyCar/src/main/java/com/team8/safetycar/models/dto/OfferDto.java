package com.team8.safetycar.models.dto;

import javax.validation.constraints.*;

import static com.team8.safetycar.utils.GlobalConstants.*;

public class OfferDto {
    @Positive(message = CAR_BRAND_ID_ERROR)
    private int carBrandId;

    @Positive(message = CAR_MODEL_ID_ERROR)
    private int carId;

    @Positive(message = CUBIC_CAPACITY_ERROR)
    private int cubicCapacity;

    @NotEmpty(message = REGISTRATION_DATE_ERROR)
    private String firstRegistrationDate;

    @Min(value = DRIVER_AGE_MIN, message = DRIVER_AGE_ERROR)
    @Max(value = DRIVER_AGE_MAX, message = DRIVER_AGE_ERROR)
    private int driverAge;

    @NotNull
    private boolean accidentsLast12Months;

    public OfferDto() {
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(int carBrandId) {
        this.carBrandId = carBrandId;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(String firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean getAccidentsLast12Months() {
        return accidentsLast12Months;
    }

    public void setAccidentsLast12Months(boolean accidentsLast12Months) {
        this.accidentsLast12Months = accidentsLast12Months;
    }
}
