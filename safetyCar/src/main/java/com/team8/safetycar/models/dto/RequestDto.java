package com.team8.safetycar.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import static com.team8.safetycar.utils.GlobalConstants.*;

public class RequestDto {
    @Size(min = PHONE_LENGTH_MIN, max = PHONE_LENGTH_MAX, message = PHONE_ERROR)
    private String phone;

    @Size(min = POSTAL_ADDRESS_LENGTH_MIN, max = POSTAL_ADDRESS_LENGTH_MAX, message = POSTAL_ADDRESS_ERROR)
    private String postalAddress;

    private MultipartFile vehicleRegistrationCertificate;

    @NotEmpty(message = START_DATE_ERROR)
    private String startDate;

    public RequestDto() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public MultipartFile getVehicleRegistrationCertificate() {
        return vehicleRegistrationCertificate;
    }

    public void setVehicleRegistrationCertificate(MultipartFile vehicleRegistrationCertificate) {
        this.vehicleRegistrationCertificate = vehicleRegistrationCertificate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
}
