package com.team8.safetycar.models.dto;

import com.team8.safetycar.annotations.Match;
import com.team8.safetycar.annotations.Password;

import javax.validation.constraints.NotBlank;

import static com.team8.safetycar.utils.GlobalConstants.*;

@Match(first = "password",
        second = "passwordConfirmation",
        message = PASSWORD_DOESNT_MATCH_ERROR)
public class UserPassEditDto {

    @Password(message = USER_PASSWORD_MESSAGE_ERROR)
    private String password;

    @NotBlank(message = PASSWORD_CONFIRMATION_REQUIRED_MESSAGE_ERROR)
    private String passwordConfirmation;

    public UserPassEditDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
