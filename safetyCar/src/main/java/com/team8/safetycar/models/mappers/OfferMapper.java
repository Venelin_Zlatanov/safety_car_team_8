package com.team8.safetycar.models.mappers;

import com.team8.safetycar.models.Car;
import com.team8.safetycar.models.Offer;
import com.team8.safetycar.models.dto.OfferDto;
import com.team8.safetycar.services.contracts.CarBrandService;
import com.team8.safetycar.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.parse;

@Component
public class OfferMapper {

    private final CarBrandService carBrandService;
    private final CarService carService;

    @Autowired
    public OfferMapper(CarBrandService carBrandService, CarService carService) {
        this.carBrandService = carBrandService;
        this.carService = carService;
    }

    public Offer toOffer(OfferDto offerDto) {
        Offer offer = new Offer();
        Car car = carService.getCarByBrandIdAndCarId(offerDto.getCarBrandId(), offerDto.getCarId());
        offer.setCar(car);
        offer.setCubicCapacity(offerDto.getCubicCapacity());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        offer.setFirstRegistrationDate(parse(offerDto.getFirstRegistrationDate(), formatter));

        offer.setDriverAge(offerDto.getDriverAge());
        offer.setAccidentsLast12Months(offerDto.getAccidentsLast12Months());
        return offer;
    }
}
