package com.team8.safetycar.models.dto;

public class FilterRequestsDto {
    private int statusId;
    private String username;

    public FilterRequestsDto() {
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
