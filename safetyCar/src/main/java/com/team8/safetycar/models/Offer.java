package com.team8.safetycar.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "offers")
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "first_registration_date")
    private LocalDate firstRegistrationDate;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "accidents_last_12_months")
    private boolean accidentsLast12Months;

    @Column(name = "total_premium")
    private double totalPremium;

    public Offer() {
    }

    public Offer(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public LocalDate getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(LocalDate firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean getAccidentsLast12Months() {
        return accidentsLast12Months;
    }

    public void setAccidentsLast12Months(boolean accidentsLast12Months) {
        this.accidentsLast12Months = accidentsLast12Months;
    }

    public double getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }
}
