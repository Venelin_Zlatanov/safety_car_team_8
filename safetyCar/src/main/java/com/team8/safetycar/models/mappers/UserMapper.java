package com.team8.safetycar.models.mappers;

import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserDtoRegistration;
import com.team8.safetycar.models.dto.UserEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    @Autowired
    public UserMapper() {
    }

    public UserDetails toUser(UserEditDto userDto, UserDetails userDetails) {
        userDetails.setFirstName(userDto.getFirstName());
        userDetails.setLastName(userDto.getLastName());
        return userDetails;
    }

    public UserDetails toUser(UserDtoRegistration userDtoRegistration) {
        UserDetails userDetails = new UserDetails();
        userDetails.setEmail(userDtoRegistration.getEmail());
        userDetails.setFirstName(userDtoRegistration.getFirstName());
        userDetails.setLastName(userDtoRegistration.getLastName());
        return userDetails;
    }

    public void toUserDetails(UserDetails userDetailsToUpdate, UserEditDto userDto) {
        userDetailsToUpdate.setFirstName(userDto.getFirstName());
        userDetailsToUpdate.setLastName(userDto.getLastName());
    }
}
