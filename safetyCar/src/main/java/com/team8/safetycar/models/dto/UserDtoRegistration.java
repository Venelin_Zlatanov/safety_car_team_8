package com.team8.safetycar.models.dto;

import com.team8.safetycar.annotations.Match;
import com.team8.safetycar.annotations.Password;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static com.team8.safetycar.utils.GlobalConstants.*;

@Match(first = "password",
        second = "passwordConfirmation",
        message = PASSWORD_DOESNT_MATCH_ERROR)
public class UserDtoRegistration {

    @NotBlank(message = EMAIL_REQUIRED_ERROR)
    @Email(message = EMAIL_INVALID_FORMAT_ERROR)
    private String email;

    @Size(min = USER_NAME_MIN_LENGTH,
            max = USER_NAME_MAX_LENGTH,
            message = USER_FIRST_NAME_MESSAGE_ERROR)
    private String firstName;

    @Size(min = USER_NAME_MIN_LENGTH,
            max = USER_NAME_MAX_LENGTH,
            message = USER_LAST_NAME_MESSAGE_ERROR)
    private String lastName;


    @Password(message = USER_PASSWORD_MESSAGE_ERROR)
    private String password;

    @NotBlank(message = PASSWORD_CONFIRMATION_REQUIRED_MESSAGE_ERROR)
    private String passwordConfirmation;

    public UserDtoRegistration() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
