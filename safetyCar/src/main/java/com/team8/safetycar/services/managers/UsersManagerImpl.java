package com.team8.safetycar.services.managers;

import com.team8.safetycar.models.ConfirmationToken;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserDtoRegistration;
import com.team8.safetycar.models.dto.UserPassEditDto;
import com.team8.safetycar.models.mappers.UserMapper;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.mail.ConfirmationTokensService;
import com.team8.safetycar.services.mail.EmailService;
import com.team8.safetycar.services.managers.contracts.UsersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.EMAIL_SUBJECT;
import static com.team8.safetycar.utils.GlobalConstants.NOT_AUTHORIZED_ERROR;

@Component
public class UsersManagerImpl implements UsersManager {

    private final UserService userService;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;
    private final ConfirmationTokensService confirmationTokensService;
    private final UserMapper userMapper;

    @Autowired
    public UsersManagerImpl(UserService userService,
                            UserDetailsManager userDetailsManager,
                            PasswordEncoder passwordEncoder,
                            EmailService emailService,
                            ConfirmationTokensService confirmationTokensService,
                            UserMapper userMapper) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.emailService = emailService;
        this.confirmationTokensService = confirmationTokensService;
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails getById(int id, UserDetails logged) {
        UserDetails userDetails = userService.getById(id);
        boolean isAdmin = userService.isAdmin(logged.getEmail());
        if (!isAdmin && id != logged.getId()) {
            throw new AccessDeniedException(NOT_AUTHORIZED_ERROR);
        }
        return userService.getById(id);
    }

    @Override
    public void createSecurityUser(UserDtoRegistration userDto, List<GrantedAuthority> authorities) {

        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getEmail(),
                        passwordEncoder.encode(userDto.getPassword()),
                        false,
                        true,
                        true,
                        true,
                        authorities);

        userDetailsManager.createUser(newUser);
    }

    @Override
    public UserDetails createUserDetails(UserDtoRegistration userDto) {
        UserDetails userDetails = userMapper.toUser(userDto);
        userService.create(userDetails);
        return userDetails;
    }

    @Override
    public void createUser(UserDtoRegistration userDto, UserDetails userDetails) {
        User user = userService.getByUsername(userDto.getEmail());
        user.setUserDetails(userDetails);
        userService.update(user);
    }

    @Override
    @Transactional
    public void sendToken(UserDetails userDetails) {
        ConfirmationToken token = confirmationTokensService.createConfirmationToken(userDetails.getEmail());
        emailService.sendConfirmationEmail(token, EMAIL_SUBJECT, "register-confirmation");
    }

    @Override
    public org.springframework.security.core.userdetails.User updateSecurityUser(UserPassEditDto userDto, Principal principal) {

        org.springframework.security.core.userdetails.UserDetails springUserDetailsToUpdate =
                userDetailsManager.loadUserByUsername(principal.getName());

        org.springframework.security.core.userdetails.User springUserToUpdate =
                new org.springframework.security.core.userdetails.User(
                        principal.getName(), passwordEncoder.encode(userDto.getPassword()), springUserDetailsToUpdate.getAuthorities());

        return springUserToUpdate;
    }
}
