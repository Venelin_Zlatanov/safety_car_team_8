package com.team8.safetycar.services.contracts;

import com.team8.safetycar.models.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);

    Status getByValue(String value);
}
