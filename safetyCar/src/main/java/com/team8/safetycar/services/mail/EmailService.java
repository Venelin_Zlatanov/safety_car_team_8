package com.team8.safetycar.services.mail;

import com.team8.safetycar.models.ConfirmationToken;
import org.thymeleaf.context.Context;

public interface EmailService {
    void sendConfirmationEmail(ConfirmationToken token, String subject, String path);

    void confirmRegistration(String confirmationToken);

    void sendChangedStatusEmail(int id, String subject, String path);

    void sendEmail(String to,
                   String from,
                   String subject,
                   String path,
                   Context context);

}
