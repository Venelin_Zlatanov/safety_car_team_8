package com.team8.safetycar.services.mail;

import com.team8.safetycar.models.ConfirmationToken;
import com.team8.safetycar.models.User;
import com.team8.safetycar.repositories.contracts.ConfirmationTokensRepository;
import com.team8.safetycar.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.UUID;

@Service
public class ConfirmationTokensServiceImpl implements ConfirmationTokensService {
    private final UserRepository usersRepository;
    private final ConfirmationTokensRepository confirmationTokensRepository;

    @Autowired
    public ConfirmationTokensServiceImpl(UserRepository usersRepository, ConfirmationTokensRepository confirmationTokensRepository) {
        this.usersRepository = usersRepository;
        this.confirmationTokensRepository = confirmationTokensRepository;
    }

    @Override
    public Collection<ConfirmationToken> getAll() {
        return confirmationTokensRepository.findAll();
    }

    @Override
    public ConfirmationToken getConfirmationTokenByName(String tokenName) {
        return confirmationTokensRepository.findByConfirmationToken(tokenName);
    }

    @Override
    @Transactional
    public ConfirmationToken createConfirmationToken(String username) {

        User user = usersRepository.getByUsername(username);

        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setUser(user);
        confirmationToken.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        confirmationToken.setConfirmationToken(UUID.randomUUID().toString());

        return confirmationTokensRepository.createConfirmationToken(confirmationToken);
    }
}
