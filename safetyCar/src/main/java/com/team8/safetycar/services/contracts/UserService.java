package com.team8.safetycar.services.contracts;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;

import java.util.List;

public interface UserService {

    List<UserDetails> getAll();

    List<UserDetails> getAllRoleUser();

    UserDetails getById(int id);

    UserDetails getByUsernameDetails(String username);

    User getByUsername(String username);

    void create(UserDetails userDetails);

    void updateDetails(UserDetails userDetails);

    void update(User userToUpdate);

    void confirmUser(String username);

    List<Request> getUserRequests(String username);

    boolean isAdmin(String username);
}
