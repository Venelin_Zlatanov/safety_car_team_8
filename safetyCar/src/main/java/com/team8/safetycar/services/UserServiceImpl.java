package com.team8.safetycar.services;

import com.team8.safetycar.exceptions.DuplicateEntityException;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.repositories.contracts.UserRepository;
import com.team8.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.USER_EMAIL_ALREADY_EXISTS_ERROR;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDetails> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<UserDetails> getAllRoleUser() {
        return userRepository.getAllRoleUser();
    }

    @Override
    public UserDetails getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public UserDetails getByUsernameDetails(String username) {
        return userRepository.getByUsernameDetails(username);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void create(UserDetails userDetails) {
        if (!userRepository.filterByEmail(userDetails.getEmail()).isEmpty()) {
            throw new DuplicateEntityException(
                    String.format(USER_EMAIL_ALREADY_EXISTS_ERROR, userDetails.getEmail())
            );
        }

        userRepository.create(userDetails);
    }

    @Override
    public void updateDetails(UserDetails userDetails) {
        userRepository.updateDetails(userDetails);
    }

    @Override
    public void update(User userToUpdate) {
        userRepository.update(userToUpdate);
    }

    @Override
    public void confirmUser(String username) {
        userRepository.confirmUser(username);
    }

    @Override
    public List<Request> getUserRequests(String username) {
        return userRepository.getUserRequests(username);
    }

    @Override
    public boolean isAdmin(String username) {
        return userRepository.isAdmin(username);
    }
}
