package com.team8.safetycar.services;

import com.team8.safetycar.models.Status;
import com.team8.safetycar.repositories.contracts.StatusRepository;
import com.team8.safetycar.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll() {
        return statusRepository.getAll();
    }

    @Override
    public Status getById(int id) {
        return statusRepository.getById(id);
    }

    @Override
    public Status getByValue(String value) {
        return statusRepository.getByValue(value);
    }
}
