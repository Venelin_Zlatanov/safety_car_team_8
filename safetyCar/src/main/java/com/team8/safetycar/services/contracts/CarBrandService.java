package com.team8.safetycar.services.contracts;

import com.team8.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandService {

    List<CarBrand> getAll();

    CarBrand getById(int id);
}
