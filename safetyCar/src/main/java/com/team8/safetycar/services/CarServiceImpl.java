package com.team8.safetycar.services;

import com.team8.safetycar.models.Car;
import com.team8.safetycar.repositories.contracts.CarRepository;
import com.team8.safetycar.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public List<Car> getAll() {
        return carRepository.getAll();
    }

    @Override
    public Car getById(int id) {
        return carRepository.getById(id);
    }

    @Override
    public List<Car> getCarsByBrandId(int carBrandId) {
        return carRepository.getCarsByBrandId(carBrandId);
    }

    @Override
    public Car getCarByBrandIdAndCarId(int carBrandId, int carId) {
        return carRepository.getCarByBrandIdAndCarId(carBrandId, carId);
    }
}
