package com.team8.safetycar.services;

import com.team8.safetycar.models.Offer;
import com.team8.safetycar.repositories.contracts.OfferRepository;
import com.team8.safetycar.services.contracts.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.*;
import static com.team8.safetycar.utils.GlobalConstants.*;

@Service
public class OfferServiceImpl implements OfferService {
    private final OfferRepository offerRepository;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public List<Offer> getAll() {
        return offerRepository.getAll();
    }

    @Override
    public Offer getById(int id) {
        return offerRepository.getById(id);
    }

    @Override
    public void create(Offer offer) {
        offerRepository.create(offer);
    }

    @Override
    public Double calculateOffer(Offer offer) {
        double netPremium = offerRepository.calculateBaseAmount(offer);

        if (offer.getAccidentsLast12Months()) {
            netPremium += netPremium * ACCIDENTS_LAST_12_MONTHS_COEFF;
        }
        if (offer.getDriverAge() < DRIVER_AGE) {
            netPremium = netPremium + netPremium * DRIVER_AGE_BELOW_25_COEFF;
        }

        return netPremium + netPremium * TAX_COEFF;
    }
}
