package com.team8.safetycar.services.managers.contracts;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.UserDetails;

import java.security.Principal;
import java.util.List;

public interface RequestsManager {

    Request getById(int id, UserDetails logged);
}
