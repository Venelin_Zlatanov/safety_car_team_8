package com.team8.safetycar.services;

import com.team8.safetycar.models.CarBrand;
import com.team8.safetycar.repositories.contracts.CarBrandRepository;
import com.team8.safetycar.services.contracts.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarBrandServiceImpl implements CarBrandService {

    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrand> getAll() {
        return carBrandRepository.getAll();
    }

    @Override
    public CarBrand getById(int id) {
        return carBrandRepository.getById(id);
    }
}
