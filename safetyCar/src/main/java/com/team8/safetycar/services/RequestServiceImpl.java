package com.team8.safetycar.services;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.repositories.contracts.RequestRepository;
import com.team8.safetycar.repositories.contracts.UserRepository;
import com.team8.safetycar.services.contracts.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final UserRepository userRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository, UserRepository userRepository) {
        this.requestRepository = requestRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Request> getAll() {
        return requestRepository.getAll();
    }

    @Override
    public Request getById(int id) {
        return requestRepository.getById(id);
    }

    @Override
    public void create(Request request) {
        requestRepository.create(request);
    }

    @Override
    public void changeStatus(Request request) {
        requestRepository.changeStatus(request);
    }

    @Override
    public List<Request> filter(int statusId, String username) {
        return requestRepository.filter(statusId, username);
    }
}
