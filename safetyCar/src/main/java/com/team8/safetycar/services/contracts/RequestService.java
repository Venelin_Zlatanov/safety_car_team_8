package com.team8.safetycar.services.contracts;

import com.team8.safetycar.models.Request;

import java.util.List;

public interface RequestService {
    List<Request> getAll();

    Request getById(int id);

    void create(Request request);

    void changeStatus(Request request);

    List<Request> filter(int statusId, String username);
}
