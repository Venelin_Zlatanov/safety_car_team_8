package com.team8.safetycar.services.managers;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.services.contracts.RequestService;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.managers.contracts.RequestsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import static com.team8.safetycar.utils.GlobalConstants.NOT_AUTHORIZED_ERROR;

@Component
public class RequestsManagerImpl implements RequestsManager {
    private final RequestService requestService;
    private final UserService userService;

    @Autowired
    public RequestsManagerImpl(RequestService requestService,
                               UserService userService) {
        this.requestService = requestService;
        this.userService = userService;
    }

    @Override
    public Request getById(int id, UserDetails logged) {
        boolean isCreator = requestService.getById(id).getUserDetails().getEmail().equals(logged.getEmail());
        boolean isAdmin = userService.isAdmin(logged.getEmail());
        if (!isAdmin && !isCreator) {
            throw new AccessDeniedException(NOT_AUTHORIZED_ERROR);
        }
        return requestService.getById(id);
    }
}
