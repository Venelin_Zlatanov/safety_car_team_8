package com.team8.safetycar.services.mail;

import com.team8.safetycar.models.ConfirmationToken;

import java.util.Collection;

public interface ConfirmationTokensService {
    Collection<ConfirmationToken> getAll();

    ConfirmationToken getConfirmationTokenByName(String tokenName);

    ConfirmationToken createConfirmationToken(String username);
}
