package com.team8.safetycar.services.mail;

import com.team8.safetycar.exceptions.EmailNotSentException;
import com.team8.safetycar.models.ConfirmationToken;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.services.contracts.RequestService;
import com.team8.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Service
@PropertySource("classpath:application.properties")
public class EmailServiceImpl implements EmailService {
    private final ConfirmationTokensService confirmationTokensService;
    private final JavaMailSender javaMailSender;
    private final SpringTemplateEngine emailTemplateEngine;
    private final UserService userService;
    private final String host;
    private final RequestService requestService;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender,
                            SpringTemplateEngine emailTemplateEngine,
                            ConfirmationTokensService confirmationTokensService,
                            UserService userService,
                            RequestService requestService,
                            Environment env) {
        this.confirmationTokensService = confirmationTokensService;
        this.javaMailSender = javaMailSender;
        this. emailTemplateEngine = emailTemplateEngine;
        this.userService = userService;
        this.requestService = requestService;

        host = env.getProperty("safetyCar.host");
    }

    @Override
    public void sendConfirmationEmail(ConfirmationToken token, String subject, String path) {
        Context context = new Context(Locale.ENGLISH);
        context.setVariable("token", token.getConfirmationToken());
        context.setVariable("host", host);

        sendEmail(token.getUser().getUsername(),
                "safetycarteam8@gmail.com",
                subject,
                path,
                context);
    }

    @Override
    public void sendChangedStatusEmail(int id, String subject, String path) {
        Request request = requestService.getById(id);
        Context context = new Context(Locale.ENGLISH);
        context.setVariable("requestId", id);
        context.setVariable("host", host);
        context.setVariable("status", request.getStatus().getValue());

        sendEmail(request.getUserDetails().getEmail(),
                "safetycarteam8@gmail.com",
                subject,
                path,
                context);
    }

    @Override
    public void sendEmail(String to, String from, String subject, String path, Context context) {
        try {
            String htmlContent = emailTemplateEngine.process("mails/" + path, context);

            MimeMessage message = javaMailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(to);
            helper.setFrom(from);
            helper.setSubject(subject);
            helper.setText(htmlContent, true);

            javaMailSender.send(message);
        } catch (Exception ex) {
            throw new EmailNotSentException(subject, to);
        }

    }

    @Override
    @Transactional
    public void confirmRegistration(String confirmationToken) {
        ConfirmationToken token = confirmationTokensService.getConfirmationTokenByName(confirmationToken);
        userService.confirmUser(token.getUser().getUsername());
    }
}
