package com.team8.safetycar.services.contracts;

import com.team8.safetycar.models.Car;

import java.util.List;

public interface CarService {

    List<Car> getAll();

    Car getById(int id);

    List<Car> getCarsByBrandId(int id);

    Car getCarByBrandIdAndCarId(int carBrandId, int carId);
}
