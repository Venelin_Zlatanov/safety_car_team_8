package com.team8.safetycar.services.managers.contracts;

import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserDtoRegistration;
import com.team8.safetycar.models.dto.UserPassEditDto;
import org.springframework.security.core.GrantedAuthority;

import java.security.Principal;
import java.util.List;

public interface UsersManager {
    void createSecurityUser(UserDtoRegistration userDto, List<GrantedAuthority> authorities);

    UserDetails createUserDetails(UserDtoRegistration userDto);

    void createUser(UserDtoRegistration userDto, UserDetails userDetails);

    void sendToken(UserDetails userDetails);

    org.springframework.security.core.userdetails.User updateSecurityUser(UserPassEditDto userDto, Principal principal);

    UserDetails getById(int id, UserDetails logged);
}
