package com.team8.safetycar.services.contracts;

import com.team8.safetycar.models.Offer;

import java.util.List;

public interface OfferService {

    List<Offer> getAll();

    Offer getById(int id);

    void create(Offer offer);

    Double calculateOffer(Offer offer);
}
