package com.team8.safetycar.controllers.mvc;

import com.team8.safetycar.models.Car;
import com.team8.safetycar.models.CarBrand;
import com.team8.safetycar.models.Offer;
import com.team8.safetycar.models.dto.OfferDto;
import com.team8.safetycar.models.mappers.OfferMapper;
import com.team8.safetycar.services.contracts.CarBrandService;
import com.team8.safetycar.services.contracts.CarService;
import com.team8.safetycar.services.contracts.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Collection;

import static com.team8.safetycar.utils.GlobalConstants.CORRECT_VALUE_MESSAGE_ERROR;
import static com.team8.safetycar.utils.RequestHelper.roundTotalPremium;

@Controller
@RequestMapping("/offers")
public class OfferController {

    private final OfferService offerService;
    private final CarBrandService carBrandService;
    private final CarService carService;
    private final OfferMapper offerMapper;

    @Autowired
    public OfferController(OfferService offerService,
                           CarBrandService carBrandService,
                           CarService carService,
                           OfferMapper offerMapper) {
        this.offerService = offerService;
        this.carBrandService = carBrandService;
        this.carService = carService;
        this.offerMapper = offerMapper;
    }

    @GetMapping("/simulate")
    public String showSimulateOfferPage(Model model) {
        model.addAttribute("offerDto", new OfferDto());
        return "requests/simulateOffer";
    }

    @PostMapping("/simulate")
    public String handleSimulateOfferPage(@Valid @ModelAttribute("offerDto") OfferDto offerDto,
                                          BindingResult bindingResult,
                                          Model model,
                                          HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", CORRECT_VALUE_MESSAGE_ERROR);
            return "requests/simulateOffer";
        }
        model.addAttribute("offerDto", offerDto);
        Offer offer = offerMapper.toOffer(offerDto);
        Double totalPremium = offerService.calculateOffer(offer);
        offer.setTotalPremium(roundTotalPremium(totalPremium));

        HttpSession session = request.getSession();
        session.setAttribute("offer", offer);
        return "redirect:/offers/new";
    }

    @GetMapping("/new")
    public String showOfferPage(Model model, HttpSession session) {
        Offer offer = (Offer) session.getAttribute("offer");
        model.addAttribute("offer", offer);
        return "requests/createOffer";
    }

    @ModelAttribute("carBrands")
    public Collection<CarBrand> populateCarBrands() {
        return carBrandService.getAll();
    }

    @ModelAttribute("cars")
    public Collection<Car> populateCars() {
        return carService.getAll();
    }
}
