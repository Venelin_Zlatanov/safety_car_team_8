package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.exceptions.DuplicateEntityException;
import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.exceptions.InvalidOperationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice(basePackages = "com.team8.safetycar.controllers.rest")
public class ExceptionHandlerRestController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<Object> exceptionAccessDenied(RuntimeException exception, WebRequest request) {
        String errorMessage = (exception != null && exception.getMessage() != null ?
                exception.getMessage() : "Unknown error.");
        return handleExceptionInternal(exception, errorMessage, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = DuplicateEntityException.class)
    public ResponseEntity<Object> handleDuplicateEntityException(RuntimeException exception, WebRequest request) {
        String errorMessage = (exception != null && exception.getMessage() != null ?
                exception.getMessage() : "Unknown error.");
        return handleExceptionInternal(exception, errorMessage, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(RuntimeException exception, WebRequest request) {
        String errorMessage = (exception != null && exception.getMessage() != null ?
                exception.getMessage() : "Unknown error.");
        return handleExceptionInternal(exception, errorMessage, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = InvalidOperationException.class)
    public ResponseEntity<Object> handleInvalidOperationException(RuntimeException exception, WebRequest request) {
        String errorMessage = (exception != null && exception.getMessage() != null ?
                exception.getMessage() : "Unknown error.");
        return handleExceptionInternal(exception, errorMessage, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                               HttpHeaders headers,
                                                               HttpStatus status,
                                                               WebRequest request) {
        String message = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return handleExceptionInternal(ex, message, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> exception(Exception exception, WebRequest request) {
        String errorMessage = (exception != null && exception.getMessage() != null ?
                exception.getMessage() : "Unknown error.");
        return handleExceptionInternal(exception, errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}