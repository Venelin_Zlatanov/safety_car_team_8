package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.exceptions.InvalidOperationException;
import com.team8.safetycar.models.Offer;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.OfferDto;
import com.team8.safetycar.models.dto.RequestDto;
import com.team8.safetycar.models.mappers.OfferMapper;
import com.team8.safetycar.models.mappers.RequestMapper;
import com.team8.safetycar.services.contracts.OfferService;
import com.team8.safetycar.services.contracts.RequestService;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.managers.contracts.RequestsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.CHANGED_STATUS_MESSAGE_ERROR;
import static com.team8.safetycar.utils.RequestHelper.roundTotalPremium;


@RestController
@RequestMapping("/api/requests")
public class RequestRestController {

    private final RequestService requestService;
    private final RequestsManager requestManager;
    private final RequestMapper requestMapper;
    private final OfferService offerService;
    private final OfferMapper offerMapper;
    private final UserService userService;


    @Autowired
    public RequestRestController(RequestService requestService,
                                 RequestsManager requestManager,
                                 RequestMapper requestMapper,
                                 OfferService offerService,
                                 OfferMapper offerMapper,
                                 UserService userService) {
        this.requestService = requestService;
        this.requestManager = requestManager;
        this.requestMapper = requestMapper;
        this.offerService = offerService;
        this.offerMapper = offerMapper;
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public List<Request> getAll() {
        return requestService.getAll();
    }

    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
    @GetMapping("/{id}")
    public Request getById(@PathVariable int id, Principal principal) {
        UserDetails logged = userService.getByUsernameDetails(principal.getName());
        return requestManager.getById(id, logged);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/my")
    public List<Request> getUserRequests(Principal principal) {
        UserDetails logged = userService.getByUsernameDetails(principal.getName());
        return userService.getUserRequests(logged.getEmail());
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping
    public Request createRequest(@RequestPart("file") MultipartFile file,
                                 @Valid @RequestPart("offer") OfferDto offerDto,
                                 @Valid @RequestPart("request") RequestDto requestDto,
                                 Principal principal) {
        Offer offer = offerMapper.toOffer(offerDto);
        Double totalPremium = offerService.calculateOffer(offer);
        offer.setTotalPremium(roundTotalPremium(totalPremium));
        offerService.create(offer);

        Offer createdOffer = offerService.getById(offer.getId());
        UserDetails logged = userService.getByUsernameDetails(principal.getName());

        requestDto.setVehicleRegistrationCertificate(file);
        Request request = requestMapper.toRequest(requestDto, createdOffer, logged);
        requestService.create(request);
        return request;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{offerId}")
    public Request createRequestWithCreatedOffer(@PathVariable int offerId,
                                                 @RequestPart("file") MultipartFile file,
                                                 @Valid @RequestPart("request") RequestDto requestDto,
                                                 Principal principal) {
        Offer offer = offerService.getById(offerId);
        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());

        requestDto.setVehicleRegistrationCertificate(file);
        Request request = requestMapper.toRequest(requestDto, offer, userDetails);
        requestService.create(request);
        return request;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/{id}")
    public Request editRequestStatus(@PathVariable int id,
                                     @RequestParam int statusId) {
        Request request = requestService.getById(id);
        if (request.getStatus().getId() != 1) {
            throw new InvalidOperationException(CHANGED_STATUS_MESSAGE_ERROR);
        }
        requestMapper.setStatus(request, statusId);
        requestService.changeStatus(request);
        return request;
    }
}
