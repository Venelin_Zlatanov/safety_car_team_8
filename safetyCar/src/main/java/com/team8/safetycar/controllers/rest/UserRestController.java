package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserEditDto;
import com.team8.safetycar.models.dto.UserPassEditDto;
import com.team8.safetycar.models.mappers.UserMapper;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.managers.contracts.UsersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserDetailsManager userDetailsManager;
    private final UsersManager usersManager;
    private final UserMapper userMapper;

    @Autowired
    public UserRestController(UserService userService,
                              UserDetailsManager userDetailsManager,
                              UsersManager usersManager,
                              UserMapper userMapper) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.usersManager = usersManager;
        this.userMapper = userMapper;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public List<UserDetails> getAllWithRoleUser() {
        return userService.getAllRoleUser();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable int id, Principal principal) {
        UserDetails logged = userService.getByUsernameDetails(principal.getName());
        return usersManager.getById(id, logged);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
    @PutMapping
    public UserDetails update(@Valid @RequestBody UserEditDto userDto, Principal principal) {
        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());
        UserDetails updatedUserDetails = userMapper.toUser(userDto, userDetails);
        userService.updateDetails(updatedUserDetails);
        return updatedUserDetails;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
    @PutMapping("/pass")
    public void pass(@Valid @RequestBody UserPassEditDto userDto, Principal principal) {
        org.springframework.security.core.userdetails.User springUserToUpdate =
                usersManager.updateSecurityUser(userDto, principal);

        userDetailsManager.updateUser(springUserToUpdate);
    }
}
