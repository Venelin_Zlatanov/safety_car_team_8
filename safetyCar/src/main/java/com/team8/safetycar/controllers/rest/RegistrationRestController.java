package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.exceptions.DuplicateEntityException;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserDtoRegistration;
import com.team8.safetycar.services.managers.contracts.UsersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.PASSWORD_DOESNT_MATCH_ERROR;
import static com.team8.safetycar.utils.GlobalConstants.USER_EMAIL_ALREADY_EXISTS_ERROR;

@RestController
@RequestMapping("/api/")
public class RegistrationRestController {
    private final UserDetailsManager userDetailsManager;
    private final UsersManager usersManager;

    @Autowired
    public RegistrationRestController(UserDetailsManager userDetailsManager, UsersManager usersManager) {
        this.userDetailsManager = userDetailsManager;
        this.usersManager = usersManager;
    }

    @PostMapping("/register")
    public void registerUser(
            @Valid @RequestBody UserDtoRegistration userDto) {

        if (userDetailsManager.userExists(userDto.getEmail())) {
            throw new DuplicateEntityException(String.format(USER_EMAIL_ALREADY_EXISTS_ERROR, userDto.getEmail()));
        }

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            throw new DuplicateEntityException(PASSWORD_DOESNT_MATCH_ERROR);
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        usersManager.createSecurityUser(userDto, authorities);
        UserDetails userDetails = usersManager.createUserDetails(userDto);
        usersManager.createUser(userDto, userDetails);
        usersManager.sendToken(userDetails);
    }
}
