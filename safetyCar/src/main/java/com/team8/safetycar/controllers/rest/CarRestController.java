package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.models.Car;
import com.team8.safetycar.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cars")
public class CarRestController {

    private final CarService carService;

    @Autowired
    public CarRestController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public List<Car> getAll() {
        return carService.getAll();
    }

    @GetMapping("/{id}")
    public Car getById(@PathVariable int id) {
        return carService.getById(id);
    }

    @GetMapping("/models/{id}")
    public List<Car> getModelByBrandId(@PathVariable int id) {
        return carService.getCarsByBrandId(id);
    }
}
