package com.team8.safetycar.controllers.mvc;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserEditDto;
import com.team8.safetycar.models.dto.UserPassEditDto;
import com.team8.safetycar.models.mappers.UserMapper;
import com.team8.safetycar.services.contracts.StatusService;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.managers.contracts.UsersManager;
import com.team8.safetycar.utils.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.*;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final UserDetailsManager userDetailsManager;
    private final StatusService statusService;
    private final UsersManager usersManager;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService,
                          UserDetailsManager userDetailsManager,
                          StatusService statusService,
                          UsersManager usersManager,
                          UserMapper userMapper) {

        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.statusService = statusService;
        this.usersManager = usersManager;
        this.userMapper = userMapper;
    }

    @GetMapping
    public String showUsers(Model model) {
        List<UserDetails> usersDetails = userService.getAllRoleUser();
        model.addAttribute("usersDetails", usersDetails);
        return "users/users";
    }

    @GetMapping("/profile")
    public String showUserProfile(Model model, Principal principal) {
        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());
        model.addAttribute("userDetails", userDetails);

        return "users/userProfile";
    }

    @GetMapping("/edit")
    public String getUserEditPage(Model model, Principal principal) {

        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());
        UserEditDto userDto = new UserEditDto();

        userDto.setFirstName(userDetails.getFirstName());
        userDto.setLastName(userDetails.getLastName());

        model.addAttribute("userDto", userDto);
        model.addAttribute("userDetails", userDetails);

        return "users/userProfileEdit";
    }

    @PostMapping("/edit")
    public String editUser(@Valid @ModelAttribute("userDto") UserEditDto userDto,
                           BindingResult bindingResult,
                           Model model,
                           Principal principal) {
        UserDetails userDetailsToUpdate = userService.getByUsernameDetails(principal.getName());
        User userToUpdate = userService.getByUsername(userDetailsToUpdate.getEmail());

        userMapper.toUserDetails(userDetailsToUpdate, userDto);
        userToUpdate.setUserDetails(userDetailsToUpdate);

        if (bindingResult.hasErrors()) {
            model.addAttribute("userDto", userDto);
            model.addAttribute("error", EMAIL_OR_PASSWORD_CAN_T_BE_EMPTY_ERROR);
            return "users/userProfileEdit";
        }

        userService.update(userToUpdate);
        userService.updateDetails(userDetailsToUpdate);

        return "redirect:/";
    }

    @GetMapping("/passEdit")
    public String getPasswordEditPage(Model model, Principal principal) {

        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());
        UserPassEditDto userDto = new UserPassEditDto();

        model.addAttribute("userDetails", userDetails);
        model.addAttribute("userDto", userDto);

        return "users/userPasswordEdit";
    }

    @PostMapping("/passEdit")
    public String passEdit(@Valid @ModelAttribute("userDto") UserPassEditDto userDto,
                           BindingResult bindingResult,
                           Model model,
                           Principal principal) {

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("userDto", userDto);
            model.addAttribute("error", PASSWORD_DOESNT_MATCH_ERROR);
            return "users/userPasswordEdit";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("userDto", userDto);
            model.addAttribute("error", PASSWORD_CANT_BE_EMPTY_ERROR);
            return "users/userPasswordEdit";
        }

        org.springframework.security.core.userdetails.User springUserToUpdate =
                usersManager.updateSecurityUser(userDto, principal);

        userDetailsManager.updateUser(springUserToUpdate);

        return "redirect:/";
    }

    @GetMapping("/requests")
    public String showAllUserRequests(Model model, Principal principal) {

        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());
        List<Request> requests = userService.getUserRequests(userDetails.getEmail());
        model.addAttribute("requests", requests);
        model.addAttribute("statuses", statusService.getAll());
        model.addAttribute("imageUtil", new ImageUtil());

        return "users/userRequest";
    }

    //Accessible only to admins
    @GetMapping("/requests/{id}")
    public String showAllUserRequests(Model model, @PathVariable int id) {

        UserDetails userDetails = userService.getById(id);
        List<Request> requests = userService.getUserRequests(userDetails.getEmail());
        model.addAttribute("requests", requests);
        model.addAttribute("statuses", statusService.getAll());
        model.addAttribute("imageUtil", new ImageUtil());

        return "users/userRequest";
    }
}


