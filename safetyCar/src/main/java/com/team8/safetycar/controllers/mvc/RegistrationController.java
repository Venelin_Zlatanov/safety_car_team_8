package com.team8.safetycar.controllers.mvc;

import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserDtoRegistration;
import com.team8.safetycar.services.mail.EmailService;
import com.team8.safetycar.services.managers.contracts.UsersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.*;

@Controller
public class RegistrationController {
    private final UserDetailsManager userDetailsManager;
    private final EmailService emailService;
    private final UsersManager usersManager;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager,
                                  EmailService emailService,
                                  UsersManager usersManager) {
        this.userDetailsManager = userDetailsManager;
        this.emailService = emailService;
        this.usersManager = usersManager;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userDto", new UserDtoRegistration());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(
            @Valid @ModelAttribute("userDto") UserDtoRegistration userDto,
            BindingResult bindingResult,
            Model model) {

        if (userDetailsManager.userExists(userDto.getEmail())) {
            model.addAttribute("userDto", userDto);
            model.addAttribute("error", String.format(USER_EMAIL_ALREADY_EXISTS_ERROR, userDto.getEmail()));
            return "register";
        }

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("userDto", userDto);
            model.addAttribute("error", PASSWORD_DOESNT_MATCH_ERROR);
            return "register";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("userDto", userDto);
            model.addAttribute("error", EMAIL_OR_PASSWORD_CAN_T_BE_EMPTY_ERROR);
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        usersManager.createSecurityUser(userDto, authorities);
        UserDetails userDetails = usersManager.createUserDetails(userDto);
        usersManager.createUser(userDto, userDetails);
        usersManager.sendToken(userDetails);
        return "users/register-confirmation";
    }

    @GetMapping("/register_confirmation")
    public String showRegisterConfirmation() {
        return "users/register-confirmation";
    }

    @GetMapping("/confirm_account")
    public String showConfirmUserAccount(@RequestParam("token") String confirmationToken) {
        emailService.confirmRegistration(confirmationToken);
        return "confirmedAccount";
    }
}
