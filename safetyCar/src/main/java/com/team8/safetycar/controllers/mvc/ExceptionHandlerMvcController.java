package com.team8.safetycar.controllers.mvc;

import com.team8.safetycar.exceptions.DuplicateEntityException;
import com.team8.safetycar.exceptions.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages = "com.team8.safetycar.controllers.mvc")
public class ExceptionHandlerMvcController {

    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public String exceptionAccessDenied() {
        return "access-denied";
    }

    @ExceptionHandler(value = {DuplicateEntityException.class})
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public String exceptionDuplicateEntityException(RuntimeException runtimeException, Model model) {
        String errorMessage = (runtimeException != null && runtimeException.getMessage() != null ?
                runtimeException.getMessage() : "Unknown error.");
        model.addAttribute("message", errorMessage);
        model.addAttribute("statusCode", HttpStatus.CONFLICT.value());
        model.addAttribute("codeTitle", HttpStatus.CONFLICT.getReasonPhrase());
        return "error";
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String exceptionEntityNotFoundException(RuntimeException runtimeException, Model model) {
        String errorMessage = (runtimeException != null && runtimeException.getMessage() != null ?
                runtimeException.getMessage() : "Unknown error.");
        model.addAttribute("message", errorMessage);
        model.addAttribute("statusCode", HttpStatus.NOT_FOUND.value());
        model.addAttribute("codeTitle", HttpStatus.NOT_FOUND.getReasonPhrase());
        return "error";
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public String exception(Exception exception, Model model) {
        String errorMessage = (exception != null && exception.getMessage() != null ?
                exception.getMessage() : "Unknown error.");
        model.addAttribute("message", errorMessage);
        model.addAttribute("exception", String.valueOf(exception));
        return "error";
    }
}