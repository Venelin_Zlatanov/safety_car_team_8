package com.team8.safetycar.controllers.mvc;

import com.team8.safetycar.models.Offer;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.Status;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.FilterRequestsDto;
import com.team8.safetycar.models.dto.RequestDto;
import com.team8.safetycar.models.dto.StatusDto;
import com.team8.safetycar.models.mappers.RequestMapper;
import com.team8.safetycar.services.contracts.OfferService;
import com.team8.safetycar.services.contracts.RequestService;
import com.team8.safetycar.services.contracts.StatusService;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.mail.EmailService;
import com.team8.safetycar.services.managers.contracts.RequestsManager;
import com.team8.safetycar.utils.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collection;

import static com.team8.safetycar.utils.GlobalConstants.*;

@Controller
@RequestMapping("/requests")
public class RequestController {

    private final RequestService requestService;
    private final OfferService offerService;
    private final UserService userService;
    private final StatusService statusService;
    private final RequestMapper requestMapper;
    private final RequestsManager requestManager;
    private final EmailService emailService;


    @Autowired
    public RequestController(RequestService requestService,
                             OfferService offerService,
                             UserService userService,
                             StatusService statusService,
                             RequestMapper requestMapper,
                             RequestsManager requestManager,
                             EmailService emailService) {
        this.requestService = requestService;
        this.offerService = offerService;
        this.userService = userService;
        this.statusService = statusService;
        this.requestMapper = requestMapper;
        this.requestManager = requestManager;
        this.emailService = emailService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public String showAllRequests(Model model) {
        model.addAttribute("requests", requestService.getAll());
        model.addAttribute("filterRequestsDto", new FilterRequestsDto());
        return "requests/requests";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    public String filterAllRequests(Model model, @ModelAttribute FilterRequestsDto dto) {
        model.addAttribute("requests", requestService.filter(dto.getStatusId(), dto.getUsername()));
        return "requests/requests";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/new")
    public String showCreateRequestPage(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            request.getSession();
            model.addAttribute("offer", new Offer());
        } else {
            Offer offer = (Offer) session.getAttribute("offer");
            model.addAttribute("offer", offer);
        }
        model.addAttribute("requestDto", new RequestDto());
        return "requests/createRequest";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/new")
    public String handleCreateRequestPage(@Valid @ModelAttribute("requestDto") RequestDto requestDto,
                                          BindingResult bindingResult,
                                          Model model,
                                          HttpSession session,
                                          Principal principal) {
        Offer offer = (Offer) session.getAttribute("offer");
        model.addAttribute("offer", offer);
        model.addAttribute("requestDto", requestDto);

        UserDetails userDetails = userService.getByUsernameDetails(principal.getName());

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", CORRECT_VALUE_MESSAGE_ERROR);
            return "requests/createRequest";
        }
        offerService.create(offer);
        Offer createdOffer = offerService.getById(offer.getId());

        Request request = requestMapper.toRequest(requestDto, createdOffer, userDetails);
        requestService.create(request);
        return "redirect:/requests/policy-confirmation";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/policy-confirmation")
    public String showPolicyConfirmation(HttpSession session) {
        session.removeAttribute("offer");
        return "requests/policy-confirmation";
    }

    @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_ADMIN')")
    @GetMapping("/{id}")
    public String showRequestDetails(@PathVariable int id, Model model, Principal principal) {
        UserDetails logged = userService.getByUsernameDetails(principal.getName());
        model.addAttribute("request", requestManager.getById(id, logged));
        model.addAttribute("statusDto", new StatusDto());
        model.addAttribute("imageUtil", new ImageUtil());
        return "requests/requestDetails";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}")
    public String editRequestStatusToApprove(@PathVariable int id) {
        changeStatus(id, "approved");
        emailService.sendChangedStatusEmail(id, EMAIL_CHANGED_STATUS_SUBJECT, "changedStatus");
        return "redirect:/requests";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{id}/reject")
    public String editRequestStatusToReject(@PathVariable int id) {
        changeStatus(id, "rejected");
        emailService.sendChangedStatusEmail(id, EMAIL_CHANGED_STATUS_SUBJECT, "changedStatus");
        return "redirect:/requests";
    }

    @ModelAttribute("statuses")
    public Collection<Status> populateStatuses() {
        return statusService.getAll();
    }

    private void changeStatus(int id, String statusValue) {
        Request request = requestService.getById(id);
        Status status = statusService.getByValue(statusValue);
        request.setStatus(status);
        requestService.changeStatus(request);
    }
}
