package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.models.Offer;
import com.team8.safetycar.models.dto.OfferDto;
import com.team8.safetycar.models.mappers.OfferMapper;
import com.team8.safetycar.services.contracts.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.team8.safetycar.utils.RequestHelper.roundTotalPremium;


@RestController
@RequestMapping("/api/offers")
public class OfferRestController {

    private final OfferService offerService;
    private final OfferMapper offerMapper;

    @Autowired
    public OfferRestController(OfferService offerService,
                               OfferMapper offerMapper) {
        this.offerService = offerService;
        this.offerMapper = offerMapper;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public List<Offer> getAll() {
        return offerService.getAll();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{id}")
    public Offer getById(@PathVariable int id) {
        return offerService.getById(id);
    }

    @PostMapping("/calculate")
    public Double calculateTotalPremium(@Valid @RequestBody OfferDto offerDto) {
        Offer offer = offerMapper.toOffer(offerDto);
        Double totalPremium = offerService.calculateOffer(offer);
        return roundTotalPremium(totalPremium);
    }

    @PostMapping("/simulate")
    public Offer simulateOffer(@Valid @RequestBody OfferDto offerDto) {
        Offer offer = offerMapper.toOffer(offerDto);
        Double totalPremium = offerService.calculateOffer(offer);
        offer.setTotalPremium(roundTotalPremium(totalPremium));
        return offer;
    }

    @PostMapping
    public Offer createOffer(@Valid @RequestBody OfferDto offerDto) {
        Offer offer = offerMapper.toOffer(offerDto);
        Double totalPremium = offerService.calculateOffer(offer);
        offer.setTotalPremium(roundTotalPremium(totalPremium));
        offerService.create(offer);
        return offer;
    }
}
