package com.team8.safetycar.controllers.rest;

import com.team8.safetycar.models.CarBrand;
import com.team8.safetycar.services.contracts.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/car-brands")
public class CarBrandRestController {

    private final CarBrandService carBrandService;

    @Autowired
    public CarBrandRestController(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }

    @GetMapping
    public List<CarBrand> getAll() {
        return carBrandService.getAll();
    }

    @GetMapping("/{id}")
    public CarBrand getById(@PathVariable int id) {
        return carBrandService.getById(id);
    }
}
