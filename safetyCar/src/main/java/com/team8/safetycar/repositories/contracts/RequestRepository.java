package com.team8.safetycar.repositories.contracts;

import com.team8.safetycar.models.Request;

import java.util.List;

public interface RequestRepository {
    List<Request> getAll();

    Request getById(int id);

    void create(Request request);

    void changeStatus(Request request);

    List<Request> filter(int statusId, String username);
}
