package com.team8.safetycar.repositories.contracts;

import com.team8.safetycar.models.Status;

import java.util.List;

public interface StatusRepository {

    List<Status> getAll();

    Status getById(int id);

    Status getByValue(String value);
}
