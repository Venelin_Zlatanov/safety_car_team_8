package com.team8.safetycar.repositories.contracts;

import com.team8.safetycar.models.Offer;

import java.util.List;

public interface OfferRepository {

    List<Offer> getAll();

    Offer getById(int id);

    void create(Offer offer);

    Double calculateBaseAmount(Offer offer);
}
