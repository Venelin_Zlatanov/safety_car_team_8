package com.team8.safetycar.repositories;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.repositories.contracts.RequestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.team8.safetycar.utils.GlobalConstants.REQUEST_ID_NOT_FOUND;

@Repository
public class RequestRepositoryImpl implements RequestRepository {

    @Autowired
    public RequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private final SessionFactory sessionFactory;

    @Override
    public List<Request> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Request> query = session.createQuery("From Request", Request.class);
            return query.list();
        }
    }

    @Override
    public List<Request> filter(int statusId, String username) {
        Set<Request> result = new HashSet<>();

        String myQuery = "select r from Request r  where 1 = 1";

        if (statusId != -1) {
            myQuery += " and r.status.id = :statusId";
        }
        if (!username.isEmpty()) {
            myQuery += " and r.userDetails.email like concat('%',:username,'%')";
        }

        myQuery += " order by r.date desc";

        try (Session session = sessionFactory.openSession()) {
            Query<Request> query = session.createQuery(myQuery, Request.class);

            if (statusId != -1) {
                query.setParameter("statusId", statusId);
            }

            if (!username.isEmpty()) {
                query.setParameter("username", username);
            }
            result.addAll(query.list());
            List<Request> requests = new ArrayList<>(result);
            return requests;
        }
    }

    @Override
    public Request getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Request request = session.get(Request.class, id);
            if (request == null) {
                throw new EntityNotFoundException(String.format(REQUEST_ID_NOT_FOUND, id));
            }
            return request;
        }
    }

    @Override
    public void create(Request request) {
        try (Session session = sessionFactory.openSession()) {
            session.save(request);
        }
    }

    @Override
    public void changeStatus(Request request) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            session.update(request);

            session.getTransaction().commit();
        }
    }
}
