package com.team8.safetycar.repositories;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.*;
import com.team8.safetycar.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Repository;

import java.util.List;
import static com.team8.safetycar.utils.GlobalConstants.*;


@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;
    private final UserDetailsManager userDetailsManager;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, UserDetailsManager userDetailsManager) {
        this.sessionFactory = sessionFactory;
        this.userDetailsManager = userDetailsManager;
    }

    @Override
    public List<UserDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails", UserDetails.class);
            return query.list();
        }
    }

    @Override
    public List<UserDetails> getAllRoleUser() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("select u from UserDetails u, Authority a where" +
                    " a.authority = 'ROLE_USER' and u.email = a.username", UserDetails.class);
            return query.list();
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails userDetails = session.get(UserDetails.class, id);
            return userDetails;
        }
    }

    @Override
    public UserDetails getByUsernameDetails(String username) {
        List<UserDetails> userDetails = this.filterByUsernameDetails(username);

        if (userDetails.isEmpty()) {
            throw new EntityNotFoundException(
                    String.format(USER_NAME_NOT_FOUND_ERROR, username)
            );
        }
        return userDetails.get(0);
    }

    @Override
    public User getByUsername(String username) {
        List<User> users = this.filterByUsername(username);
        return users.get(0);
    }

    @Override
    public List<UserDetails> filterByUsernameDetails(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where email = :username", UserDetails.class);
            query.setParameter("username", username);
            return query.list();
        }
    }

    @Override
    public List<User> filterByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            return query.list();
        }
    }

    @Override
    public List<UserDetails> filterByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where email = :email", UserDetails.class);
            query.setParameter("email", email);
            return query.list();
        }
    }

    @Override
    public void create(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateDetails(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean isAdmin(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Authority> query = session.createQuery("from Authority where username = :username", Authority.class);
            query.setParameter("username", username);
            return query.list().get(0).getAuthority().equals("ROLE_ADMIN");
        }
    }

    @Override
    public void confirmUser(String username) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = filterByUsername(username).get(0);
            UserDetails userDetails = filterByUsernameDetails(username).get(0);

            user.setActive(1);
            userDetails.setActive(1);

            session.update(userDetails);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Request> getUserRequests(String username) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails userDetails = filterByUsernameDetails(username).get(0);
            int id = userDetails.getId();
            Query<Request> query = session.createQuery("from Request where userDetails.id = :id ORDER BY date desc", Request.class);
            query.setParameter("id", id);
            return query.list();
        }
    }
}
