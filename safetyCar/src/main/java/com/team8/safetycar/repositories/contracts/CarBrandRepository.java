package com.team8.safetycar.repositories.contracts;

import com.team8.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandRepository {

    List<CarBrand> getAll();

    CarBrand getById(int id);
}
