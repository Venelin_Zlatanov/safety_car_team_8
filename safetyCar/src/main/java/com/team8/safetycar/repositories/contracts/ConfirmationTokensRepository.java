package com.team8.safetycar.repositories.contracts;

import com.team8.safetycar.models.ConfirmationToken;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfirmationTokensRepository {
    ConfirmationToken findByConfirmationToken(String token);

    List<ConfirmationToken> filterByToken(String token);

    List<ConfirmationToken> findAll();

    ConfirmationToken createConfirmationToken(ConfirmationToken confirmationToken);
}
