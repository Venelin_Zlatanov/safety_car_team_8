package com.team8.safetycar.repositories;

import com.team8.safetycar.models.ConfirmationToken;
import com.team8.safetycar.repositories.contracts.ConfirmationTokensRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConfirmationTokenRepositoryImpl implements ConfirmationTokensRepository {

    private final SessionFactory sessionFactory;

    public ConfirmationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ConfirmationToken createConfirmationToken(ConfirmationToken confirmationToken) {

        try (Session session = sessionFactory.openSession()) {
            session.save(confirmationToken);
            return confirmationToken;
        }
    }

    @Override
    public ConfirmationToken findByConfirmationToken(String token) {
        List<ConfirmationToken> confirmationTokens = this.filterByToken(token);
        return confirmationTokens.get(0);
    }

    @Override
    public List<ConfirmationToken> filterByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<ConfirmationToken> query = session.createQuery(
                    "from ConfirmationToken where confirmationToken = :token", ConfirmationToken.class);
            query.setParameter("token", token);
            return query.list();
        }
    }

    @Override
    public List<ConfirmationToken> findAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ConfirmationToken> query = session.createQuery(
                    "from ConfirmationToken ", ConfirmationToken.class);
            return query.list();
        }
    }
}
