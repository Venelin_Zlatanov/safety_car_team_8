package com.team8.safetycar.repositories;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.CarBrand;
import com.team8.safetycar.repositories.contracts.CarBrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.CAR_BRAND_ID_NOT_FOUND;

@Repository
public class CarBrandRepositoryImpl implements CarBrandRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarBrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarBrand> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarBrand> query = session.createQuery("from CarBrand ", CarBrand.class);
            return query.list();
        }
    }

    @Override
    public CarBrand getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarBrand carBrand = session.get(CarBrand.class, id);
            if (carBrand == null) {
                throw new EntityNotFoundException(String.format(CAR_BRAND_ID_NOT_FOUND, id));
            }
            return carBrand;
        }
    }
}
