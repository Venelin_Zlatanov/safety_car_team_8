package com.team8.safetycar.repositories.contracts;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;

import java.util.List;


public interface UserRepository {

    List<UserDetails> getAll();

    List<UserDetails> getAllRoleUser();

    UserDetails getById(int id);

    UserDetails getByUsernameDetails(String username);

    User getByUsername(String username);

    List<UserDetails> filterByUsernameDetails(String username);

    List<User> filterByUsername(String username);

    List<UserDetails> filterByEmail(String email);

    void create(UserDetails userDetails);

    void updateDetails(UserDetails userDetails);

    void update(User user);

    boolean isAdmin(String username);

    void confirmUser(String username);

    List<Request> getUserRequests(String username);
}
