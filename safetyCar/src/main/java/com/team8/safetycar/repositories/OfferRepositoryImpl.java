package com.team8.safetycar.repositories;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.MultiCriteriaRange;
import com.team8.safetycar.models.Offer;
import com.team8.safetycar.repositories.contracts.OfferRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.OFFER_ID_NOT_FOUND;


@Repository
public class OfferRepositoryImpl implements OfferRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public OfferRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Offer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Offer> query = session.createQuery("from Offer", Offer.class);
            return query.list();
        }
    }

    @Override
    public Offer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Offer offer = session.get(Offer.class, id);
            if (offer == null) {
                throw new EntityNotFoundException(String.format(OFFER_ID_NOT_FOUND, id));
            }
            return offer;
        }
    }

    @Override
    public void create(Offer offer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(offer);
        }
    }

    @Override
    public Double calculateBaseAmount(Offer offer) {
        try (Session session = sessionFactory.openSession()) {

            LocalDate firstRegistrationDate = offer.getFirstRegistrationDate();
            Period diff = Period.between(firstRegistrationDate, LocalDate.now());
            int carAge = diff.getYears();

            Query<MultiCriteriaRange> query = session.createQuery(
                    "from MultiCriteriaRange m where :cc between m.ccMin and m.ccMax and :carAge between m.carAgeMin and m.carAgeMax"
                    , MultiCriteriaRange.class);

            query.setParameter("cc", offer.getCubicCapacity());
            query.setParameter("carAge", carAge);

            return query.list().get(0).getBaseAmount();
        }
    }
}
