package com.team8.safetycar.repositories;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.Car;
import com.team8.safetycar.models.CarBrand;
import com.team8.safetycar.repositories.contracts.CarRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.*;


@Repository
public class CarRepositoryImpl implements CarRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Car> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car", Car.class);
            return query.list();
        }
    }

    @Override
    public Car getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Car car = session.get(Car.class, id);
            if (car == null) {
                throw new EntityNotFoundException(String.format(CAR_WITH_ID_NOT_FOUND, id));
            }
            return car;
        }
    }

    @Override
    public List<Car> getCarsByBrandId(int carBrandId) {
        try (Session session = sessionFactory.openSession()) {
            CarBrand carBrand = session.get(CarBrand.class, carBrandId);
            if (carBrand == null) {
                throw new EntityNotFoundException(String.format(BRAND_WITH_ID_NOT_FOUND, carBrandId));
            }
            Query<Car> query = session.createQuery("from Car where carBrand.id = :id", Car.class);
            query.setParameter("id", carBrandId);
            return query.list();
        }
    }

    @Override
    public Car getCarByBrandIdAndCarId(int carBrandId, int carId) {
        try (Session session = sessionFactory.openSession()) {
            CarBrand carBrand = session.get(CarBrand.class, carBrandId);
            if (carBrand == null) {
                throw new EntityNotFoundException(String.format(BRAND_WITH_ID_NOT_FOUND, carBrandId));
            }
            Query<Car> query = session.createQuery("select c from Car c, CarBrand b where " +
                    "b.id = :carBrandId and c.id = :carId and c.carBrand.id = b.id", Car.class);
            query.setParameter("carBrandId", carBrandId);
            query.setParameter("carId", carId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException(String.format(CAR_ID_AND_BRAND_ID_NOT_FOUND, carId, carBrandId));
            }
            return query.list().get(0);
        }
    }
}
