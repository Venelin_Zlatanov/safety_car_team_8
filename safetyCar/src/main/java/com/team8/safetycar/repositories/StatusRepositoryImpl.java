package com.team8.safetycar.repositories;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.Status;
import com.team8.safetycar.repositories.contracts.StatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.team8.safetycar.utils.GlobalConstants.STATUS_ID_NOT_FOUND;

@Repository
public class StatusRepositoryImpl implements StatusRepository {

    SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status ", Status.class);
            return query.list();
        }
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class, id);
            if (status == null) {
                throw new EntityNotFoundException(String.format(STATUS_ID_NOT_FOUND, id));
            }
            return status;
        }
    }

    @Override
    public Status getByValue(String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status where value = :value", Status.class);
            query.setParameter("value", value);
            List<Status> statusList = query.list();
            return statusList.get(0);
        }
    }
}
