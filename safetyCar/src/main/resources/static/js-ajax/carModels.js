$(document).ready(function () {
    loadCarBrand();

    $("select#inputGroupSelect03").change(function () {
        console.log("this", $(this));
        var id = $(this).children("option:selected").val();
        console.log("id", id);
        clearModels();
        loadModels(id);
    });
});

function loadCarBrand() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/car-brands',
        success: function (data) {
            console.log("carBrand");
            console.log(data);
            $.each(data, function (key, value) {
                var html = createOptionHtml(value.id, value.name);
                console.log($('#inputGroupSelect03'));
                $('#inputGroupSelect03').append(html);
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function loadModels(id) {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/cars/models/' + id,
        success: function (data) {
            console.log(data);
            $('#inputGroupSelect01').append('<option selected value="-2">...</option>');
            $.each(data, function (key, value) {
                var html = createOptionHtml(value.id, value.model);
                console.log(html);
                $('#inputGroupSelect01').append(html);
            });
        }
    });
}

function clearModels() {
    $('#inputGroupSelect01').empty();
}

function createOptionHtml(key, value) {
    return '<option value="' + key + '">' + value + '</option>';
}

