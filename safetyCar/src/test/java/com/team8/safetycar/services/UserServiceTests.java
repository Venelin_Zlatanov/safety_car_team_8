package com.team8.safetycar.services;

import com.team8.safetycar.exceptions.DuplicateEntityException;
import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.helpers.Factory;
import com.team8.safetycar.models.Authority;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository mockUserRepository;

    @Test
    public void getAll_Should_Return_ListOfUsers() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(Arrays.asList(mockUser));

        // Act
        List<UserDetails> list = userService.getAll();

        // Assert
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void getAll_Should_Return_EmptyListOfUsers() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        List<UserDetails> list = userService.getAll();

        // Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getAllRoleUser_Should_Return_ListOfUsers() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getAllRoleUser())
                .thenReturn(Arrays.asList(mockUser));

        // Act
        List<UserDetails> list = userService.getAllRoleUser();

        // Assert
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void getAllShould_Return_EmptyListOfUsers() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getAllRoleUser())
                .thenReturn(new ArrayList<>());

        // Act
        List<UserDetails> list = userService.getAllRoleUser();

        // Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getById_Should_Throw_WhenUserNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.getById(anyInt()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getById(anyInt()));
    }

    @Test
    public void getById_Should_CallRepository() {
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(mockUser);

        // Act
        userService.getById(1);

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getById(1);
    }

    @Test
    public void getByUsernameDetails_Throw_WhenUserNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.getByUsernameDetails(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsernameDetails(anyString()));
    }

    @Test
    public void getByUsernameDetails_Should_CallRepository() {
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getByUsernameDetails(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act
        userService.getByUsernameDetails(mockUser.getEmail());

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getByUsernameDetails(mockUser.getEmail());
    }

    @Test
    public void getByUsername_Throw_WhenUserNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.getByUsername(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsername(anyString()));
    }

    @Test
    public void getByUsername_Should_CallRepository() {
        User mockUser = Factory.createMockUser();
        Mockito.when(mockUserRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act
        userService.getByUsername(mockUser.getUsername());

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getByUsername(mockUser.getUsername());
    }

    @Test
    public void create_Should_Throw_WhenUserExists() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.filterByEmail(mockUser.getEmail()))
                .thenReturn(Arrays.asList(mockUser));

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void create_ShouldCreate_WhenUserDoesNotExist() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.filterByEmail(mockUser.getEmail()))
                .thenReturn(new ArrayList<>());

        // Act
        userService.create(mockUser);

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).create(mockUser);
    }

    @Test
    public void updateDetails_ShouldUpdate_WhenUserIsAdmin() {
        // Arrange
        User mockUser = Factory.createMockUser();
        mockUser.setAuthorities(new HashSet<>(Arrays.asList(new Authority("ROLE_ADMIN"))));

        // Act
        userService.update(mockUser);

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(mockUser);
    }

    @Test
    public void confirmUser_Should_Should_CallRepository() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();

        // Act
        userService.confirmUser(mockUser.getEmail());

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).confirmUser(mockUser.getEmail());
    }

    @Test
    public void getUserRequests_Should_Return_ListOfRequests() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Request mockRequest = new Request();
        Mockito.when(mockUserRepository.getUserRequests(mockUser.getEmail()))
                .thenReturn(Arrays.asList(mockRequest));

        // Act
        List<Request> list = userService.getUserRequests(mockUser.getEmail());

        // Assert
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void getUserRequests_Should_Return_EmptyListOfRequests() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserRepository.getUserRequests(mockUser.getEmail()))
                .thenReturn(new ArrayList<>());

        // Act
        List<Request> list = userService.getUserRequests(mockUser.getEmail());

        // Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void isAdmin_Should_CallRepository() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();

        // Act
        userService.isAdmin(mockUser.getEmail());

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).isAdmin(mockUser.getEmail());
    }


}
