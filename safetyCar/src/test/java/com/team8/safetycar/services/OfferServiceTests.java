package com.team8.safetycar.services;

import com.team8.safetycar.models.Offer;
import com.team8.safetycar.repositories.contracts.OfferRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class OfferServiceTests {

    @InjectMocks
    OfferServiceImpl offerService;

    @Mock
    OfferRepository mockOfferRepository;

    @Test
    public void getAll_Should_ReturnAllOffers() {
        // Arrange
        List<Offer> offers = new ArrayList<>();
        Mockito.when(mockOfferRepository.getAll())
                .thenReturn(offers);

        List<Offer> offersToVerify = offerService.getAll();

        // Act, Assert
        Assertions.assertEquals(offersToVerify, offers);
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Offer offer = new Offer(1);
        ;
        Mockito.when(mockOfferRepository.getById(1))
                .thenReturn(offer);

        // Act
        Offer offerToVerify = offerService.getById(1);

        // Assert
        Assertions.assertEquals(offerToVerify, offer);
    }

    @Test
    public void create_ShouldCreate() {
        // Arrange
        Offer offer = new Offer(1);

        // Act
        offerService.create(offer);

        // Assert
        Mockito.verify(mockOfferRepository,
                Mockito.times(1)).create(offer);
    }

    @Test
    public void calculateOffer_ShouldCalculate() {
        // Arrange
        Offer offer = new Offer(1);

        // Act
        offerService.calculateOffer(offer);

        // Assert
        Mockito.verify(mockOfferRepository,
                Mockito.times(1)).calculateBaseAmount(offer);
    }
}
