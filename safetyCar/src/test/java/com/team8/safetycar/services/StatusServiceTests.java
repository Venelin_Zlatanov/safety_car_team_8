package com.team8.safetycar.services;

import com.team8.safetycar.models.Status;
import com.team8.safetycar.repositories.contracts.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatusServiceTests {

    @InjectMocks
    StatusServiceImpl statusService;

    @Mock
    StatusRepository mockStatusRepository;


    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Status status = new Status(1, "pending");
        Mockito.when(mockStatusRepository.getById(1))
                .thenReturn(status);

        // Act
        Status statusToVerify = statusService.getById(1);

        // Assert
        Assertions.assertEquals(statusToVerify, status);
    }

    @Test
    public void getByValue_ShouldGetValue() {
        // Arrange
        Status status = new Status(1, "pending");
        Mockito.when(mockStatusRepository.getByValue("pending"))
                .thenReturn(status);

        // Act
        Status statusToVerify = statusService.getByValue("pending");

        // Act, Assert
        Assertions.assertEquals(statusToVerify, status);
    }
}
