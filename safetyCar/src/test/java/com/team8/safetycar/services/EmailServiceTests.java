package com.team8.safetycar.services;

import com.team8.safetycar.exceptions.EmailNotSentException;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.mail.ConfirmationTokensService;
import com.team8.safetycar.services.mail.EmailServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;

import static com.team8.safetycar.helpers.Factory.createContext;
import static com.team8.safetycar.helpers.Factory.createMimeMessage;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTests {

    @Spy
    SpringTemplateEngine spySpringTemplateEngine;

    @Mock
    JavaMailSender mockMailSender;

    @Mock
    ConfirmationTokensService confirmationTokensService;

    @Mock
    UserService userService;

    @Mock
    Environment env;


    @InjectMocks
    EmailServiceImpl mockService;

    @Test
    public void sendEmailShould_CallJavaMailSender() {
        // Arrange
        Mockito.when(mockMailSender.createMimeMessage()).thenReturn(createMimeMessage());

        // Act
        mockService.sendEmail("to", "from", "subject", "path", createContext());

        // Assert
        Mockito.verify(mockMailSender, Mockito.times(1))
                .send(Mockito.any(MimeMessage.class));
    }

    @Test
    public void sendEmailShould_Trow_WhenContextIsNull() {
        // Arrange, Act, Assert
        Assertions.assertThrows(EmailNotSentException.class,
                () -> mockService.sendEmail("to", "from", "subject", "path", null));

        Mockito.verify(mockMailSender, Mockito.times(0)).createMimeMessage();
    }
}
