package com.team8.safetycar.services;

import com.team8.safetycar.models.Car;
import com.team8.safetycar.repositories.contracts.CarRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarServiceTests {

    @InjectMocks
    CarServiceImpl carService;

    @Mock
    CarRepository mockCarRepository;

    @Test
    public void getAll_Should_ReturnAllCar() {
        // Arrange
        List<Car> cars = new ArrayList<>();
        Mockito.when(mockCarRepository.getAll())
                .thenReturn(cars);

        List<Car> carsToVerify = carService.getAll();

        // Act, Assert
        Assertions.assertEquals(carsToVerify, cars);
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Car car = new Car(1, "testCar");
        ;
        Mockito.when(mockCarRepository.getById(1))
                .thenReturn(car);

        // Act
        Car carToVerify = carService.getById(1);

        // Assert
        Assertions.assertEquals(carToVerify, car);
    }

    @Test
    public void getModelById_ShouldReturn_WhenValidId() {
        // Arrange
        Car car = new Car(1, "testCar");
        List<Car> cars = new ArrayList<>();
        cars.add(car);

        Mockito.when(mockCarRepository.getCarsByBrandId(1))
                .thenReturn(cars);

        // Act
        Car returnedCar = carService.getCarsByBrandId(1).get(0);

        // Assert
        Assertions.assertEquals(returnedCar, car);
    }
}
