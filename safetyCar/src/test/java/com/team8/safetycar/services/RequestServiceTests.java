package com.team8.safetycar.services;

import com.team8.safetycar.models.Request;
import com.team8.safetycar.repositories.contracts.RequestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class RequestServiceTests {

    @InjectMocks
    RequestServiceImpl requestService;

    @Mock
    RequestRepository mockRequestRepository;

    @Test
    public void getAll_Should_ReturnAllRequests() {
        // Arrange
        List<Request> requests = new ArrayList<>();
        Mockito.when(mockRequestRepository.getAll())
                .thenReturn(requests);

        List<Request> requestsToVerify = requestService.getAll();

        // Act, Assert
        Assertions.assertEquals(requestsToVerify, requests);
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Request request = new Request(1);
        Mockito.when(mockRequestRepository.getById(1))
                .thenReturn(request);

        // Act
        Request requestToVerify = requestService.getById(1);

        // Assert
        Assertions.assertEquals(requestToVerify, request);
    }

    @Test
    public void create_ShouldCreate() {
        // Arrange
        Request request = new Request(1);

        // Act
        requestService.create(request);

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).create(request);
    }

    @Test
    public void change_Status_ShouldChange() {
        // Arrange
        Request request = new Request(1);

        // Act
        requestService.changeStatus(request);

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).changeStatus(request);
    }

    @Test
    public void filter_Should_Filter() {
        // Arrange

        List<Request> requests = new ArrayList<>();

        Mockito.when(mockRequestRepository.filter(1, ""))
                .thenReturn(requests);

        List<Request> requestsToVerify = requestService.filter(1, "");

        // Act, Assert
        Assertions.assertEquals(requestsToVerify, requests);
    }
}
