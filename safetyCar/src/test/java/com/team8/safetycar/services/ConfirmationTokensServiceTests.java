package com.team8.safetycar.services;

import com.team8.safetycar.exceptions.EntityNotFoundException;
import com.team8.safetycar.models.ConfirmationToken;
import com.team8.safetycar.models.User;
import com.team8.safetycar.repositories.contracts.ConfirmationTokensRepository;
import com.team8.safetycar.repositories.contracts.UserRepository;
import com.team8.safetycar.services.mail.ConfirmationTokensServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.team8.safetycar.helpers.Factory.createMockUser;
import static com.team8.safetycar.helpers.Factory.createToken;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class ConfirmationTokensServiceTests {
    @Mock
    ConfirmationTokensRepository mockRepository;

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    ConfirmationTokensServiceImpl mockService;

    @Test
    public void getAllShould_ReturnListOfConfirmationTokens() {
        // Arrange
        Mockito.when(mockRepository.findAll())
                .thenReturn(Arrays.asList(createToken()));

        // Act
        Collection<ConfirmationToken> list = mockService.getAll();

        // Assert
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void getAllShould_ReturnEmptyListOfConfirmationTokens() {
        // Arrange
        Mockito.when(mockRepository.findAll())
                .thenReturn(Collections.emptyList());

        // Act
        Collection<ConfirmationToken> list = mockService.getAll();

        // Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getConfirmationTokenByNameShould_ReturnConfirmationToken_WhenConfirmationTokenExists() {
        // Arrange
        ConfirmationToken expected = createToken();

        Mockito.when(mockRepository.findByConfirmationToken(anyString()))
                .thenReturn(expected);

        // Act
        ConfirmationToken actual = mockService.getConfirmationTokenByName(anyString());

        // Assert
        Assertions.assertSame(expected, actual);
    }

    @Test
    public void getConfirmationTokenByNameShould_CallRepository() {
        // Arrange
        ConfirmationToken expected = createToken();

        Mockito.when(mockRepository.findByConfirmationToken(anyString()))
                .thenReturn(expected);

        // Act
        mockService.getConfirmationTokenByName(anyString());

        // Assert
        Mockito.verify(mockRepository,
                times(1)).findByConfirmationToken(anyString());
    }

    @Test
    public void createConfirmationTokenShould_ReturnConfirmationToken() {
        // Arrange
        User user = createMockUser();
        ConfirmationToken expected = createToken();

        Mockito.when(mockUserRepository.getByUsername(anyString()))
                .thenReturn(user);

        Mockito.when(mockRepository.createConfirmationToken(any(ConfirmationToken.class))).thenReturn(expected);

        // Act
        ConfirmationToken actual = mockService.createConfirmationToken(anyString());

        // Assert
        Assertions.assertSame(expected, actual);
    }

    @Test
    public void createCategoryShould_Throw_WhenUserNotExist() {
        // Arrange
        Mockito.when(mockUserRepository.getByUsername(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.createConfirmationToken(anyString()));
    }
}
