package com.team8.safetycar.services;

import com.team8.safetycar.models.CarBrand;
import com.team8.safetycar.repositories.contracts.CarBrandRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarBrandServiceTests {

    @InjectMocks
    CarBrandServiceImpl carBrandService;

    @Mock
    CarBrandRepository mockCarBrandRepository;

    @Test
    public void getAll_Should_ReturnAllCarBrands() {
        // Arrange
        List<CarBrand> carBrands = new ArrayList<>();
        Mockito.when(mockCarBrandRepository.getAll())
                .thenReturn(carBrands);

        List<CarBrand> carBrandsToVerify = carBrandService.getAll();

        // Act, Assert
        Assertions.assertEquals(carBrandsToVerify, carBrands);
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        CarBrand carBrand = new CarBrand(1, "testCarBrand");
        ;
        Mockito.when(mockCarBrandRepository.getById(1))
                .thenReturn(carBrand);

        // Act
        CarBrand carBrandToVerify = carBrandService.getById(1);

        // Assert
        Assertions.assertEquals(carBrandToVerify, carBrand);
    }
}
