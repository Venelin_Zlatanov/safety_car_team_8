package com.team8.safetycar.managers;

import com.team8.safetycar.helpers.Factory;
import com.team8.safetycar.models.Authority;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.services.contracts.RequestService;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.managers.RequestsManagerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;

import java.util.Arrays;
import java.util.HashSet;

import static com.team8.safetycar.helpers.Factory.createMockRequest;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
public class RequestManagerTests {

    @InjectMocks
    RequestsManagerImpl mockManager;

    @Mock
    RequestService mockRequestService;

    @Mock
    UserService mockUserService;

    @Test
    public void getById_Should_Return_Request() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Request mockRequest = new Request();
        mockRequest.setUserDetails(mockUser);
        Mockito.when(mockRequestService.getById(anyInt()))
                .thenReturn(mockRequest);
        Mockito.when(mockUserService.isAdmin(mockUser.getEmail()))
                .thenReturn(true);

        // Act
        Request request = mockManager.getById(anyInt(), mockUser);

        // Assert


        Assertions.assertEquals(request, mockRequest);
    }

    @Test
    public void getById_Should_Throw_WhenUserIsNotAuthorized() {
        // Arrange
        User mockUser = Factory.createMockUser();
        mockUser.setAuthorities(new HashSet<>(Arrays.asList(new Authority("ROLE_USER"))));
        UserDetails mockUserDetails = Factory.createMockUserDetails();
        mockUserDetails.setEmail("test.test@yahoo.com");
        Request request = createMockRequest();
        Mockito.when(mockRequestService.getById(anyInt())).thenReturn(request);


        // Act, Assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> mockManager.getById(1, mockUserDetails));
    }
}
