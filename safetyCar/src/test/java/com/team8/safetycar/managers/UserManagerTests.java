package com.team8.safetycar.managers;

import com.team8.safetycar.helpers.Factory;
import com.team8.safetycar.models.Authority;
import com.team8.safetycar.models.Request;
import com.team8.safetycar.models.User;
import com.team8.safetycar.models.UserDetails;
import com.team8.safetycar.models.dto.UserDtoRegistration;
import com.team8.safetycar.models.mappers.UserMapper;
import com.team8.safetycar.services.contracts.UserService;
import com.team8.safetycar.services.managers.UsersManagerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
public class UserManagerTests {

    @InjectMocks
    UsersManagerImpl mockManager;

    @Mock
    UserService mockUserService;

    @Mock
    UserMapper mockUserMapper;

    @Test
    public void getById_Should_getId() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserService.getById(anyInt()))
                .thenReturn(mockUser);
        Mockito.when(mockUserService.isAdmin(mockUser.getEmail()))
                .thenReturn(true);

        // Act
        UserDetails userDetails = mockManager.getById(anyInt(), mockUser);

        // Assert


        Assertions.assertEquals(userDetails, mockUser);
    }

    @Test
    public void getById_Should_Throw_WhenUserNotAdmin() {
        // Arrange
        UserDetails mockUser = Factory.createMockUserDetails();
        Mockito.when(mockUserService.isAdmin(mockUser.getEmail()))
                .thenReturn(false);

        // Act, Assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> mockManager.getById(anyInt(), mockUser));
    }

    @Test
    public void createUserDetails_ShouldCreate() {
        // Arrange
        UserDtoRegistration mockDto = new UserDtoRegistration();
        UserDetails mockUserDetails = Factory.createMockUserDetails();

        Mockito.when(mockUserMapper.toUser(mockDto))
                .thenReturn(mockUserDetails);
        // Act
        UserDetails userDetails = mockManager.createUserDetails(mockDto);

        // Assert
        Assertions.assertEquals(userDetails, mockUserDetails);
    }

    @Test
    public void createUser_ShouldCreate() {
        // Arrange
        UserDtoRegistration mockDto = new UserDtoRegistration();
        UserDetails mockUserDetails = Factory.createMockUserDetails();
        User mockUser = new User();

        Mockito.when(mockUserService.getByUsername(mockDto.getEmail()))
                .thenReturn(mockUser);
        // Act

        mockManager.createUser(mockDto, mockUserDetails);
        // Assert
        Mockito.verify(mockUserService,
                Mockito.times(1)).update(mockUser);
    }
}
