package com.team8.safetycar.helpers;

import com.team8.safetycar.models.*;
import org.thymeleaf.context.Context;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.Properties;


public class Factory {

    public static UserDetails createMockUserDetails() {
        return new UserDetails(1, "test@gmail.com");
    }

    public static User createMockUser() {
        return new User("test333@gmail.com", new UserDetails(2, "test222@gmail.com"));
    }

    public static MimeMessage createMimeMessage() {
        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);
        return new MimeMessage(session);
    }

    public static Context createContext() {
        ConfirmationToken token = createToken();
        Context context = new Context(Locale.ENGLISH);
        context.setVariable("token", token.getConfirmationToken());
//        context.setVariable("host", "safetyCar.host");
        return context;
    }

    public static ConfirmationToken createToken() {
        return new ConfirmationToken(1, "Token", new Timestamp(System.currentTimeMillis()), createMockUser());
    }

    public static Offer createMockOffer() {
        return new Offer();
    }

    public static Request createMockRequest() {
        return new Request(1, createMockUserDetails());
    }
}
