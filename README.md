# SAFETY CAR web application
### Team 8
#### Members: Venelin Zlatanov, Violeta Dimitrova

Safety car:  
https://safetycarteameight.herokuapp.com

Rest API documentation:  
https://safetycarteameight.herokuapp.com/swagger-ui.html

Trello board:  
https://trello.com/b/LJbKnWUP/safetycarteam8

A full-stack Web application for car insurance which has three types of users: public, private and administrators.  
## Project Description
Safety Car is an insurance-oriented application for the end-users. It allows simulation of the amount and request insurance policies online.  
The public part of the application is visible without authentication. It provides the following functionalities:  
- Simulate of car insurance offer based on some input criteria  
- Creation of a new account and login functionality  
- Completion of registration via an email confirmation link  
- Provide constraints to create a safe password  
  
The private part of the application is for registered users only and is accessible after successful login.
The main functionalities provided for this area are:  
- Send request for approval of offer (request policy)  
- List history of user’s requests  
 
The administrators of the system have permission to manage all major information objects in the system. 
The main functionalities provided for this area are:  
- List of requests created in the system  
- Approval or rejection of the requests  
- Possibility to filter requests by user and status (pending, reject or approve) 

## Tehnology
- JDK version 11  
- SpringMVC and SpringBoot framework    
- Spring Security  
- Hibernate
- Thymeleaf  
- MariaDB  
- Swagger  
- Bootstrap Template  

## Database diagram
![database diagram](dataBase/DataBase.JPG)  
 
## UI  
#### Home
![home](images/Home.JPG)

#### Log in 
![log in](images/LogIn.JPG)  

#### Sign up  
![sign up](images/SignUp.JPG)  

#### Simulate Offer  
![simulate offer](images/SimulateOffer.JPG)

#### My Requests  
![my requests](images/MyRequests.JPG)  

#### Admin  
![admin](images/Admin.JPG)  


